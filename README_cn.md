# 图像超分辨率网络应用
本项目在Atlas200DK上，实现了对SRCNN、FSRCNN和ESPCN三种图像超分辨率网络的推理功能。

# 目录说明
* doc目录包含所有的项目文档，建议查看pdf版本以避免可能出现的排版混乱、公式缺失等情况
* sample-superresolution为项目目录，包含可直接在C30版本下运行的代码和模型文件


部署指导请参考sample-superresolution目录下的README_cn.md, 更多资料可参考doc目录下的相关文档

请使用 1.3.0.0 版本MindStudio